#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
	FILE* fichier = NULL;

	// Ouverture du fichier poubelle en mode ecriture uniquement
	fichier = fopen("/dev/null", "w");

	if (fichier != NULL){
		// Ouverture réussi
		// printf("%s\n", argp[0]);
		// Ecrire dans le fichier
		fputs("Hello World !", fichier);
		// fermeture du fichier une fois qu'on y a écrit dedans
		fclose(fichier);
	} else {
		printf("Impossible d'ouvrir le fichier\n");
	}
	return 0;
}