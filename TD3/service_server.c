/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "service.h"

int *
write_message_1_svc(char **argp, struct svc_req *rqstp)
{
	static int  result;

	FILE* fichier = NULL;

	// Ouverture du fichier poubelle en mode ecriture uniquement
	fichier = fopen("/dev/null", "w");

	if (fichier != NULL){
		// Ouverture réussi
		printf("%s\n", argp[0]);
		// Ecrire dans le fichier
		fputs(argp[0], fichier);
		// fermeture du fichier une fois qu'on y a écrit dedans
		fclose(fichier);
		result = 0;
	} else {
		result = -1;
	}

	return &result;
}
