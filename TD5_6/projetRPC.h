#ifndef PROJET_RPC
#define PROJET_RPC

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
// #include <auth_unix.h>

int input(char *input, char *data, int maxSize);

// Commande ls
type_nom getNomListe(const cell_nom *cell);
liste_nom getSuivantListe(const cell_nom *cell);
int getErreurListe(const ls_res *result);
liste_nom getListe(const ls_res *result);

// Commande read
type_bloc getBlocFichier(const cell_bloc *cell);
liste_bloc getSuivantFichier(const cell_bloc *cell);
int getErreurFichier(const read_res *result);
liste_bloc getFichier(const read_res *result);

// Commande write
bool_t getEcraserWrite(write_parm *argp);
type_nom getNomWrite(write_parm *argp);
liste_bloc getDonneesWrite(write_parm *argp);

#endif /* !PROJET_RPC */