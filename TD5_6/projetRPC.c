#include "proto.h"

// Vide le buffer
void viderBuffer() {
	int c = 0;
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
}

// Permet de stocker dans une variable passee en parametre une entree au clavier
int input(char *input, char *data, int maxSize) {
	printf("%s : ", input);
	char *startPos = NULL;
	if (fgets(data, maxSize, stdin) != NULL) {
		startPos = strchr(data, '\n');
		if (startPos != NULL) {
			*startPos = '\0';
		} else {
			viderBuffer();
		}
		return 0;
	} else {
		viderBuffer();
		return 1;
	}
}


// Commande ls
type_nom getNomListe(const cell_nom *cell) {
	return cell->nom;
}

liste_nom getSuivantListe(const cell_nom *cell) {
	return cell->suivant;
}

int getErreurListe(const ls_res *result) {
	return result->erreur;
}

liste_nom getListe(const ls_res *result) {
	return result->ls_res_u.liste;
}


// Commande read
type_bloc getBlocFichier(const cell_bloc *cell) {
	return cell->bloc;
}

liste_bloc getSuivantFichier(const cell_bloc *cell) {
	return cell->suivant;
}

int getErreurFichier(const read_res *result) {
	return result->erreur;
}

liste_bloc getFichier(const read_res *result) {
	return result->read_res_u.fichier;
}


// Commande write
bool_t getEcraserWrite(write_parm *argp) {
	return argp->ecraser;
}

type_nom getNomWrite(write_parm *argp) {
	return argp->nom;
}

liste_bloc getDonneesWrite(write_parm *argp) {
	return argp->donnees;
}
