struct param {
	int borne_inf;
	int borne_max;
};

program SOMME_PROG
{
	
	version SOMME_VERS
	{
		int SOMME_COMPUTING(param) = 1;
	}  = 1;
} = 0x20000001;