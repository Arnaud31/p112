#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int min, max, somme;

	// Recuperation des arguments, conversion en int
	min = atoi(argv[1]);
	max = atoi(argv[2]);
	somme = 0;

	// Somme des entiers entre min et max
	for (int i = min; i <= max; ++i)
	{
		somme += i;
	}
	printf("Somme : %d\n", somme);
	return 0;
}